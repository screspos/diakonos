<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ParticipantesActividad $model */

$this->title = Yii::t('app', 'Create Participantes Actividad');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Participantes Actividads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participantes-actividad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
