<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Predicas $model */

$this->title = Yii::t('app', 'Create Predicas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Predicas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="predicas-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <h2 style="color:red">¡Importante!</h2>
    <p style="color:red">La prédica tiene que expandir los puntos que ha desarrollado el predicador el domingo
    en el culto, no ser una copia del mensaje.</p>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
