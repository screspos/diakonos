<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use app\models\Discipulos;



AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ]
    ]);
    
    
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
       
        'items' => !Yii::$app->user->isGuest ? [
                [
                'label' => 'Modelos Principales',
                'items' => [
                    ['label' => 'Iglesias', 'url' => ['/iglesias/index']],
                    ['label' => 'Líneas', 'url' => ['/lineas/index']],
                    ['label' => 'Células', 'url' => ['/celulas/index']],
                    ['label' => 'Actividades', 'url' => ['/actividades/index']],
                    ['label' => 'Discípulos', 'url' => ['/discipulos/index']],
                    ['label' => 'Prédicas', 'url' => ['/predicas/index']],
                    ['label' => 'Ubicaciones', 'url' => ['/ubicaciones/index']]
                ]
            ], 
            [
                'label' => 'Modelos Secundarios',
                'items' => [
                    ['label' => 'Organizadores', 'url' => ['/organizadores/index']],
                    ['label' => 'Participantes en Actividad', 'url' => ['/participantes-actividad/index']],
                    ['label' => 'Participantes en Ministerio', 'url' => ['/participantes-ministerio/index']],
                    ['label' => 'Observaciones', 'url' => ['/observaciones/index']],
                    ['label' => 'Versículos', 'url' => ['/versiculos/index']],
                ]    
            ],
            '<li>'
            . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
            . Html::submitButton(
                'Logout ( ' . Discipulos::currentNombre() . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
             
        ]:
        [['label' => 'Login', 'url' => '']
            ]
        
        
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; Saúl Crespo Saldaña. 2º FP Desarrollo de Aplicaciones Multiplataforma @ CEINMARK Santander <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
