<?php
echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
       
        'items' => [
            'label' => 'home', 'url' => ['site/index'],
            'label' => 'planeamiento', 'url' => ['site/index'],
            'label' => 'informe', 'url' => ['site/index'],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            ) 
        ]
    ]);

