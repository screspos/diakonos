<?php
use yii\helpers\Html;
use app\models\Discipulos;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Celulas;
use yii\helpers\Url;

/** @var yii\web\View $this */




$this->title = 'Diakonos - Home';
?>
<div class="site-index">
    
    <div id="index" class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Bienvenido a Diakonos</h1>
        <h2><?= $nombre?></h2>
        <h2>Bienvenido <?=Discipulos::currentNombre()?></h2>
        <p class="lead"><i>
            Buscad, pues, hermanos, de entre vosotros a siete varones de buen
             testimonio, llenos del Espíritu Santo y de sabiduría, a quienes
             encarguemos de este trabajo. Y nosotros persistiremos en la oración
             y en el ministerio de la palabra.</i></p>
        <p><b>Hechos 6:3-4</b></p>
        <div class="row">
            <div class="col-md-3">
                <p>La aplicación se encuentra ahora mismo en construcción.
                    Cuando el desarrollo haya finalizado, usted podrá:</p>
                <?= Html::img('@web/img/under_construction.jpeg', ['alt' => 'Example Image', 'class' => 'img-fluid']) ?>
            </div>

            <div class="col-md-3">

                <p>Organizar las actividades de su iglesia y a sus líderes y 
                discípulos de manera más eficiente y efectiva.</p>
                <?= Html::img('@web/img/organizar.jpeg', ['alt' => 'Example Image', 'class' => 'img-fluid']) ?>

            </div>
            <div class="col-md-3">
               <p>Analizar los datos de las actividades que se realizan para
               encontrar las necesidades y factores a mejorar.</p>
               <?= Html::img('@web/img/analizar.jpeg', ['alt' => 'Example Image', 'class' => 'img-fluid']) ?>
            </div>
            <div class="col-md-3">
                <p>Utilizar el tiempo que ahorrará en gestión para profundizar
                en su relación con Dios, pasándolo en oración y estudio de
                Su Palabra.</p>
                <?= Html::img('@web/img/estudiarbiblia.jpeg', ['alt' => 'Example Image', 'class' => 'img-fluid']) ?>

            </div>
        </div>
        <?= GridView::widget([
        'dataProvider' => $celulas,
        'columns' => [
            'ID',
            'nombre',
            'lider',
            'linea',
            'ubicacion',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Celulas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'ID' => $model->ID]);
                 }
            ],
        ],
    ]); ?>
    </div>
    
    <?= $this->render('_chart', []) ?>
</div>
