<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Versiculos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="versiculos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'predica')->textInput() ?>

    <?= $form->field($model, 'versiculo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
