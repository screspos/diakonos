<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Versiculos $model */

$this->title = Yii::t('app', 'Create Versiculos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Versiculos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="versiculos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
