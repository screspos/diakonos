<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ParticipantesMinisterio $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="participantes-ministerio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'discipulo')->textInput() ?>

    <?= $form->field($model, 'ministerio')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
