<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ParticipantesMinisterio $model */

$this->title = Yii::t('app', 'Create Participantes Ministerio');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Participantes Ministerios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participantes-ministerio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
