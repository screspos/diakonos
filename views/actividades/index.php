<?php

use app\models\Actividades;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'Actividades');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="actividades-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Actividades'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'tipo',
            'fecha',
            'hora',
            'descripcion:ntext',
            //'resumen:ntext',
            //'ofrenda',
            //'gastos',
            //'ubicacion',
            //'iglesia',
            //'documentador',
            //'fecha_entrega',
            //'linea',
            //'celula',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Actividades $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'ID' => $model->ID]);
                 }
            ],
        ],
    ]); ?>


</div>
