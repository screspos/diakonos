<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Actividades $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="actividades-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tipo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'hora')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'resumen')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ofrenda')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gastos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ubicacion')->textInput() ?>

    <?= $form->field($model, 'iglesia')->textInput() ?>

    <?= $form->field($model, 'documentador')->textInput() ?>

    <?= $form->field($model, 'fecha_entrega')->textInput() ?>

    <?= $form->field($model, 'linea')->textInput() ?>

    <?= $form->field($model, 'celula')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
