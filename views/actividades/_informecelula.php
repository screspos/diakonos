<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;

/** @var yii\web\View $this */
/** @var app\models\Actividades $model */
/** @var ActiveForm $form */

$model->tipo = "Reunión célula";
$model->celula = $celula->id;
$model->linea = $celula->linea;
$model->documentador = Yii::$app->user->id;

// En teoría este código debería coger la ID de la iglesia.
$dataProvider = new ActiveDataProvider([
    'query' => Iglesias::find()->select("id")->where("id=" . $celula->linea)
]);
$record = $dataProvider->getModels()[0] ?? null;
$model->iglesia = $record ? $record['iglesia'] : null;
?>
<div class="form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'fecha') ?>
        <?= $form->field($model, 'hora') ?>
        <?= $form->field($model, 'fecha_entrega') ?>
        <?= $form->field($model, 'descripcion') ?>
        <?= $form->field($model, 'ofrenda') ?>
        <?= $form->field($model, 'iglesia') ?>

    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- form -->
