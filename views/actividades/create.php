<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Actividades $model */

$this->title = Yii::t('app', 'Create Actividades');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Actividades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="actividades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
