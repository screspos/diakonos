<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ubicaciones $model */

$this->title = Yii::t('app', 'Create Ubicaciones');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ubicaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ubicaciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
