<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Discipulos $model */

$this->title = Yii::t('app', 'Create Discipulos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Discipulos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discipulos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
