<?php

use app\models\Discipulos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'Discipulos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discipulos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Discipulos'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'nombre',
            'email:email',
            'sexo',
            'fecha_nac',
            //'cod_celula',
            //'bautizado',
            //'nivel_estudios',
            //'telefono',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Discipulos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'ID' => $model->ID]);
                 }
            ],
        ],
    ]); ?>


</div>
