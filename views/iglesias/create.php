<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Iglesias $model */

$this->title = Yii::t('app', 'Create Iglesias');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Iglesias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iglesias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
