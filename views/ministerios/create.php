<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ministerios $model */

$this->title = Yii::t('app', 'Create Ministerios');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ministerios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ministerios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
