<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Organizadores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="organizadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ministerio')->textInput() ?>

    <?= $form->field($model, 'actividad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
