<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Organizadores $model */

$this->title = Yii::t('app', 'Create Organizadores');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Organizadores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organizadores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
