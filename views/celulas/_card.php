<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<div class="card col-md-6">
    <div><!-- Mensaje importante -->
        
    </div>
    <div class="d-flex justify-content-between"><!-- Texto + Mapa -->
        <div class="mp"><!-- Texto -->
            <h6>CÉLULA <?= $model["nombre"] ?></h6>
            <h6>LÍDER <?= $model["nombre_lider"] ?></h6>
            <h6>LÍNEA <?= $model["nombre_linea"] ?></h6>
            <h6>IGLESIA <?= $model["localidad"] ?></h6>
            <h6><?= $model["direccion"] . ', ' . $model["localidad"]  . ', ' . $model["provincia"]  . ', ' . $model["pais"] ?></h6>
        </div>
        <div class=""><!-- Mapa -->
            <?= Html::img('@web/img/mockmap.png', ['alt' => 'Example Image', 'class' => 'img-thumbnail']) ?>
        </div>
    </div>
    <div class="d-flex justify-content-between"><!-- Columnas de botones -->
        <div class="d-flex justify-content-between align-items-around flex-wrap">
            <p><?= Html::a('Rellenar Informe',['actividades/informe'],['class'=>'btn btn-primary'])?></p>
            <p><?= Html::a('Ver Histórico',Url::to(['celulas/informacion', 'celula' => $model["ID"]],['class'=>'btn btn-warning']))?></p>
        
            <p><?= Html::a('Crear Planeamiento',['actividades/plancelula'],['class'=>'btn btn-primary'])?></p>
            <p><?= Html::a('Ver Planeamiento',['actividades/verplancelula'],['class'=>'btn btn-warning'])?></p>
        
            <p><?= Html::a('Añadir Discípulo',['discipulos/add'],['class'=>'btn btn-primary'])?></p>
            <p><?= Html::a('Ver Discípulos',['discipulos/index'],['class'=>'btn btn-warning'])?></p>
        </div>
    </div>
</div>

