<?php

use yii\widgets\ListView;


$this->title = 'Células';

?>
<main class="margin d-flex justify-content-center">
    

 <?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_card', // Name of the subview template for each item
    'layout' => '{items}', // Remove unnecessary layout elements
    'itemOptions' => ['class' => 'celula-info'], // Add a CSS class for styling
]) ?>
</main>