<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Celulas $model */

$this->title = Yii::t('app', 'Create Celulas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Celulas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="celulas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
