<?php
use yii\widgets\ListView;
use app\helpers\Buttons;
/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

?>
<h2 class="jumbotron text-center"><?= $celula?></h2>


<p><?=Buttons::return('btn btn-warning')?></p>
<div class="container">
    <?= Listview::widget([
       'dataProvider' => $actividades,
       'itemView' => '_reunion', // Name of the subview template for each item
       'layout' => '{items}', // Remove unnecessary layout elements
       'itemOptions' => ['class' => 'celula-info'], // Add a CSS class for styling     
    ])?>
    
</div>