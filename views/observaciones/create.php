<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Observaciones $model */

$this->title = Yii::t('app', 'Create Observaciones');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Observaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="observaciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
