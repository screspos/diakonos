<?php

namespace app\controllers;

use app\models\Celulas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Discipulos;
use yii\data\SqlDataProvider;

/**
 * CelulasController implements the CRUD actions for Celulas model.
 */
class CelulasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Celulas models.
     *
     * @return string
     */
    public function actionGrid()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Celulas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'ID' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('grid', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Celulas model.
     * @param int $ID ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($ID)
    {
        return $this->render('view', [
            'model' => $this->findModel($ID),
        ]);
    }

    /**
     * Creates a new Celulas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Celulas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'ID' => $model->ID]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Celulas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $ID ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($ID)
    {
        $model = $this->findModel($ID);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ID' => $model->ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Celulas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $ID ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($ID)
    {
        $this->findModel($ID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Celulas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $ID ID
     * @return Celulas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($ID)
    {
        if (($model = Celulas::findOne(['ID' => $ID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
    public function actionIndex()
    {
        $dataProvider = new SqlDataProvider([
            'sql' => 
                'SELECT DISTINCT c.nombre, l.nombre AS nombre_linea, u.direccion AS direccion,
                 i.localidad AS localidad, i.provincia AS provincia, i.pais AS pais,
                 d.nombre AS nombre_lider, c.ID
                 FROM iglesias i INNER JOIN lineas l ON i.ID = l.iglesia
                   INNER JOIN celulas c ON l.ID = c.linea
                   INNER JOIN discipulos d ON c.lider = d.ID
                   INNER JOIN ubicaciones u ON c.ubicacion=u.ID'
//                   WHERE d.id=' . Discipulos::currentID() 
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionInformacion($celula) {
        $actividades = new SqlDataProvider([
                'sql' =>
           'SELECT DISTINCT a.fecha as fecha, a.hora as hora,
               a.ofrenda as ofrenda, p.titulo as titulo, a.id AS actividad,
               p.id AS predica, a.fecha_entrega as entrega
            FROM actividades a
              INNER JOIN predicas p ON a.ID=p.actividad
              INNER JOIN participantes_actividad pa ON pa.actividad=a.ID
            WHERE a.celula = ' . $celula  .
//              AND a.tipo="Célula"
          '  ORDER BY a.fecha, a.hora' 
        ]);
        
        return $this->render('informacion',[
            'celula' => $this->findModel($celula)->nombre,
            'actividades' => $actividades
        ]);
    }
    
}
