<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
namespace app\helpers;

use yii\helpers\Html;
use Yii;

class Buttons {
    /**
     * Botón para volver a la página anterior.
     * 
     * @param type $bootstrap El estilo que va a tener el botón.
     * @return HTML String con un enlace a la últmia página visitada.
     */
    public static function return($bootstrap) {
        return Html::a('Volver',Yii::$app->request->referrer?:Yii::$app->homeUrl, ['class' => $bootstrap]);
    }
}

