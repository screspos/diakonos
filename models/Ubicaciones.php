<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ubicaciones".
 *
 * @property int $ID
 * @property string|null $nombre
 * @property string|null $direccion
 * @property int|null $iglesia
 *
 * @property Actividades[] $actividades
 * @property Celulas[] $celulas
 * @property Iglesias $iglesia0
 */
class Ubicaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ubicaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iglesia'], 'integer'],
            [['nombre', 'direccion'], 'string', 'max' => 50],
            [['iglesia'], 'exist', 'skipOnError' => true, 'targetClass' => Iglesias::class, 'targetAttribute' => ['iglesia' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'iglesia' => 'Iglesia',
        ];
    }

    /**
     * Gets query for [[Actividades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividades()
    {
        return $this->hasMany(Actividades::class, ['ubicacion' => 'ID']);
    }

    /**
     * Gets query for [[Celulas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCelulas()
    {
        return $this->hasMany(Celulas::class, ['ubicacion' => 'ID']);
    }

    /**
     * Gets query for [[Iglesia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIglesia0()
    {
        return $this->hasOne(Iglesias::class, ['ID' => 'iglesia']);
    }
}
