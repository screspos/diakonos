<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "actividades".
 *
 * @property int $ID
 * @property string|null $tipo
 * @property string|null $fecha
 * @property string|null $hora
 * @property string|null $descripcion
 * @property string|null $resumen
 * @property float|null $ofrenda
 * @property float|null $gastos
 * @property int|null $ubicacion
 * @property int|null $iglesia
 * @property int|null $documentador
 * @property string|null $fecha_entrega
 * @property int|null $linea
 * @property int|null $celula
 *
 * @property Celulas $celula0
 * @property Discipulos[] $discipulos
 * @property Discipulos $documentador0
 * @property Iglesias $iglesia0
 * @property Lineas $linea0
 * @property Ministerios[] $ministerios
 * @property Organizadores[] $organizadores
 * @property ParticipantesActividad[] $participantesActividads
 * @property Discipulos[] $predicadors
 * @property Predicas[] $predicas
 * @property Ubicaciones $ubicacion0
 */
class Actividades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'actividades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora', 'fecha_entrega'], 'safe'],
            [['descripcion', 'resumen'], 'string'],
            [['ofrenda', 'gastos'], 'number'],
            [['ubicacion', 'iglesia', 'documentador', 'linea', 'celula'], 'integer'],
            [['tipo'], 'string', 'max' => 40],
            [['celula'], 'exist', 'skipOnError' => true, 'targetClass' => Celulas::class, 'targetAttribute' => ['celula' => 'ID']],
            [['documentador'], 'exist', 'skipOnError' => true, 'targetClass' => Discipulos::class, 'targetAttribute' => ['documentador' => 'ID']],
            [['iglesia'], 'exist', 'skipOnError' => true, 'targetClass' => Iglesias::class, 'targetAttribute' => ['iglesia' => 'ID']],
            [['linea'], 'exist', 'skipOnError' => true, 'targetClass' => Lineas::class, 'targetAttribute' => ['linea' => 'ID']],
            [['ubicacion'], 'exist', 'skipOnError' => true, 'targetClass' => Ubicaciones::class, 'targetAttribute' => ['ubicacion' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'tipo' => Yii::t('app', 'Tipo'),
            'fecha' => Yii::t('app', 'Fecha'),
            'hora' => Yii::t('app', 'Hora'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'resumen' => Yii::t('app', 'Resumen'),
            'ofrenda' => Yii::t('app', 'Ofrenda'),
            'gastos' => Yii::t('app', 'Gastos'),
            'ubicacion' => Yii::t('app', 'Ubicacion'),
            'iglesia' => Yii::t('app', 'Iglesia'),
            'documentador' => Yii::t('app', 'Documentador'),
            'fecha_entrega' => Yii::t('app', 'Fecha Entrega'),
            'linea' => Yii::t('app', 'Linea'),
            'celula' => Yii::t('app', 'Celula'),
        ];
    }

    /**
     * Gets query for [[Celula0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCelula0()
    {
        return $this->hasOne(Celulas::class, ['ID' => 'celula']);
    }

    /**
     * Gets query for [[Discipulos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipulos()
    {
        return $this->hasMany(Discipulos::class, ['ID' => 'discipulo'])->viaTable('participantes_actividad', ['actividad' => 'ID']);
    }

    /**
     * Gets query for [[Documentador0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentador0()
    {
        return $this->hasOne(Discipulos::class, ['ID' => 'documentador']);
    }

    /**
     * Gets query for [[Iglesia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIglesia0()
    {
        return $this->hasOne(Iglesias::class, ['ID' => 'iglesia']);
    }

    /**
     * Gets query for [[Linea0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLinea0()
    {
        return $this->hasOne(Lineas::class, ['ID' => 'linea']);
    }

    /**
     * Gets query for [[Ministerios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMinisterios()
    {
        return $this->hasMany(Ministerios::class, ['ID' => 'ministerio'])->viaTable('organizadores', ['actividad' => 'ID']);
    }

    /**
     * Gets query for [[Organizadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizadores()
    {
        return $this->hasMany(Organizadores::class, ['actividad' => 'ID']);
    }

    /**
     * Gets query for [[ParticipantesActividads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipantesActividads()
    {
        return $this->hasMany(ParticipantesActividad::class, ['actividad' => 'ID']);
    }

    /**
     * Gets query for [[Predicadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPredicadors()
    {
        return $this->hasMany(Discipulos::class, ['ID' => 'predicador'])->viaTable('predicas', ['actividad' => 'ID']);
    }

    /**
     * Gets query for [[Predicas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPredicas()
    {
        return $this->hasMany(Predicas::class, ['actividad' => 'ID']);
    }

    /**
     * Gets query for [[Ubicacion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUbicacion0()
    {
        return $this->hasOne(Ubicaciones::class, ['ID' => 'ubicacion']);
    }
}
