<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "participantes_ministerio".
 *
 * @property int $ID
 * @property int|null $discipulo
 * @property int|null $ministerio
 *
 * @property Discipulos $discipulo0
 * @property Ministerios $ministerio0
 */
class ParticipantesMinisterio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'participantes_ministerio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['discipulo', 'ministerio'], 'integer'],
            [['discipulo', 'ministerio'], 'unique', 'targetAttribute' => ['discipulo', 'ministerio']],
            [['discipulo'], 'exist', 'skipOnError' => true, 'targetClass' => Discipulos::class, 'targetAttribute' => ['discipulo' => 'ID']],
            [['ministerio'], 'exist', 'skipOnError' => true, 'targetClass' => Ministerios::class, 'targetAttribute' => ['ministerio' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'discipulo' => 'Discipulo',
            'ministerio' => 'Ministerio',
        ];
    }

    /**
     * Gets query for [[Discipulo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipulo0()
    {
        return $this->hasOne(Discipulos::class, ['ID' => 'discipulo']);
    }

    /**
     * Gets query for [[Ministerio0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMinisterio0()
    {
        return $this->hasOne(Ministerios::class, ['ID' => 'ministerio']);
    }
}
