<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "participantes_actividad".
 *
 * @property int $ID
 * @property int|null $discipulo
 * @property int|null $actividad
 *
 * @property Actividades $actividad0
 * @property Discipulos $discipulo0
 */
class ParticipantesActividad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'participantes_actividad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['discipulo', 'actividad'], 'integer'],
            [['discipulo', 'actividad'], 'unique', 'targetAttribute' => ['discipulo', 'actividad']],
            [['actividad'], 'exist', 'skipOnError' => true, 'targetClass' => Actividades::class, 'targetAttribute' => ['actividad' => 'ID']],
            [['discipulo'], 'exist', 'skipOnError' => true, 'targetClass' => Discipulos::class, 'targetAttribute' => ['discipulo' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'discipulo' => 'Discipulo',
            'actividad' => 'Actividad',
        ];
    }

    /**
     * Gets query for [[Actividad0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividad0()
    {
        return $this->hasOne(Actividades::class, ['ID' => 'actividad']);
    }

    /**
     * Gets query for [[Discipulo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipulo0()
    {
        return $this->hasOne(Discipulos::class, ['ID' => 'discipulo']);
    }
}
