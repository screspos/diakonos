<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "discipulos".
 *
 * @property int $ID
 * @property string|null $nombre
 * @property string|null $email
 * @property int|null $sexo
 * @property string|null $fecha_nac
 * @property int|null $cod_celula
 * @property int|null $bautizado
 * @property string|null $nivel_estudios
 * @property string|null $telefono
 *
 * @property Actividades[] $actividades
 * @property Actividades[] $actividads
 * @property Actividades[] $actividads0
 * @property Celulas[] $celulas
 * @property Celulas $codCelula
 * @property Iglesias[] $iglesias
 * @property Lineas[] $lineas
 * @property Ministerios[] $ministerios
 * @property Ministerios[] $ministerios0
 * @property Observaciones[] $observaciones
 * @property ParticipantesActividad[] $participantesActividads
 * @property ParticipantesMinisterio[] $participantesMinisterios
 * @property Predicas[] $predicas
 */
class Discipulos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'discipulos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sexo', 'cod_celula', 'bautizado'], 'integer'],
            [['fecha_nac'], 'safe'],
            [['nombre', 'email'], 'string', 'max' => 40],
            [['nivel_estudios'], 'string', 'max' => 20],
            [['telefono'], 'string', 'max' => 15],
            [['cod_celula'], 'exist', 'skipOnError' => true, 'targetClass' => Celulas::class, 'targetAttribute' => ['cod_celula' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'email' => Yii::t('app', 'Email'),
            'sexo' => Yii::t('app', 'Sexo'),
            'fecha_nac' => Yii::t('app', 'Fecha Nac'),
            'cod_celula' => Yii::t('app', 'Cod Celula'),
            'bautizado' => Yii::t('app', 'Bautizado'),
            'nivel_estudios' => Yii::t('app', 'Nivel Estudios'),
            'telefono' => Yii::t('app', 'Telefono'),
        ];
    }

    /**
     * Gets query for [[Actividades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividades()
    {
        return $this->hasMany(Actividades::class, ['documentador' => 'ID']);
    }

    /**
     * Gets query for [[Actividads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividads()
    {
        return $this->hasMany(Actividades::class, ['ID' => 'actividad'])->viaTable('participantes_actividad', ['discipulo' => 'ID']);
    }

    /**
     * Gets query for [[Actividads0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividads0()
    {
        return $this->hasMany(Actividades::class, ['ID' => 'actividad'])->viaTable('predicas', ['predicador' => 'ID']);
    }

    /**
     * Gets query for [[Celulas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCelulas()
    {
        return $this->hasMany(Celulas::class, ['lider' => 'ID']);
    }

    /**
     * Gets query for [[CodCelula]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCelula()
    {
        return $this->hasOne(Celulas::class, ['ID' => 'cod_celula']);
    }

    /**
     * Gets query for [[Iglesias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIglesias()
    {
        return $this->hasMany(Iglesias::class, ['pastor_id' => 'ID']);
    }

    /**
     * Gets query for [[Lineas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLineas()
    {
        return $this->hasMany(Lineas::class, ['lider' => 'ID']);
    }

    /**
     * Gets query for [[Ministerios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMinisterios()
    {
        return $this->hasMany(Ministerios::class, ['lider' => 'ID']);
    }

    /**
     * Gets query for [[Ministerios0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMinisterios0()
    {
        return $this->hasMany(Ministerios::class, ['ID' => 'ministerio'])->viaTable('participantes_ministerio', ['discipulo' => 'ID']);
    }

    /**
     * Gets query for [[Observaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObservaciones()
    {
        return $this->hasMany(Observaciones::class, ['discipulo' => 'ID']);
    }

    /**
     * Gets query for [[ParticipantesActividads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipantesActividads()
    {
        return $this->hasMany(ParticipantesActividad::class, ['discipulo' => 'ID']);
    }

    /**
     * Gets query for [[ParticipantesMinisterios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipantesMinisterios()
    {
        return $this->hasMany(ParticipantesMinisterio::class, ['discipulo' => 'ID']);
    }

    /**
     * Gets query for [[Predicas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPredicas()
    {
        return $this->hasMany(Predicas::class, ['predicador' => 'ID']);
    }
    
    public static function currentNombre() {
        return Yii::$app->user->identity->discipulo->nombre;
    }
    
    public static function currentID() {
        return Yii::$app->user->identity->discipulo->ID;
    }

}
