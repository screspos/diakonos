<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "predicas".
 *
 * @property int $ID
 * @property string|null $titulo
 * @property string|null $resumen
 * @property int|null $predicador
 * @property int|null $actividad
 *
 * @property Actividades $actividad0
 * @property Discipulos $predicador0
 * @property Versiculos[] $versiculos
 */
class Predicas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'predicas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['resumen'], 'string'],
            [['predicador', 'actividad'], 'integer'],
            [['titulo'], 'string', 'max' => 30],
            [['predicador', 'actividad'], 'unique', 'targetAttribute' => ['predicador', 'actividad']],
            [['actividad'], 'exist', 'skipOnError' => true, 'targetClass' => Actividades::class, 'targetAttribute' => ['actividad' => 'ID']],
            [['predicador'], 'exist', 'skipOnError' => true, 'targetClass' => Discipulos::class, 'targetAttribute' => ['predicador' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'titulo' => 'Titulo',
            'resumen' => 'Resumen',
            'predicador' => 'Predicador',
            'actividad' => 'Actividad',
        ];
    }

    /**
     * Gets query for [[Actividad0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividad0()
    {
        return $this->hasOne(Actividades::class, ['ID' => 'actividad']);
    }

    /**
     * Gets query for [[Predicador0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPredicador0()
    {
        return $this->hasOne(Discipulos::class, ['ID' => 'predicador']);
    }

    /**
     * Gets query for [[Versiculos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVersiculos()
    {
        return $this->hasMany(Versiculos::class, ['predica' => 'ID']);
    }
}
