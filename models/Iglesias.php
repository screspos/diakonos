<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "iglesias".
 *
 * @property int $ID
 * @property string|null $pais
 * @property string|null $provincia
 * @property string|null $localidad
 * @property float|null $arcas
 * @property int|null $pastor_id
 *
 * @property Actividades[] $actividades
 * @property Lineas[] $lineas
 * @property Ministerios[] $ministerios
 * @property Discipulos $pastor
 * @property Ubicaciones[] $ubicaciones
 */
class Iglesias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'iglesias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['arcas'], 'number'],
            [['pastor_id'], 'integer'],
            [['pais'], 'string', 'max' => 25],
            [['provincia', 'localidad'], 'string', 'max' => 30],
            [['pastor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discipulos::class, 'targetAttribute' => ['pastor_id' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'pais' => 'Pais',
            'provincia' => 'Provincia',
            'localidad' => 'Localidad',
            'arcas' => 'Arcas',
            'pastor_id' => 'Pastor ID',
        ];
    }

    /**
     * Gets query for [[Actividades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividades()
    {
        return $this->hasMany(Actividades::class, ['iglesia' => 'ID']);
    }

    /**
     * Gets query for [[Lineas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLineas()
    {
        return $this->hasMany(Lineas::class, ['iglesia' => 'ID']);
    }

    /**
     * Gets query for [[Ministerios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMinisterios()
    {
        return $this->hasMany(Ministerios::class, ['iglesia' => 'ID']);
    }

    /**
     * Gets query for [[Pastor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPastor()
    {
        return $this->hasOne(Discipulos::class, ['ID' => 'pastor_id']);
    }

    /**
     * Gets query for [[Ubicaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUbicaciones()
    {
        return $this->hasMany(Ubicaciones::class, ['iglesia' => 'ID']);
    }
}
