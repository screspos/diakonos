<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organizadores".
 *
 * @property int $ID
 * @property int|null $ministerio
 * @property int|null $actividad
 *
 * @property Actividades $actividad0
 * @property Ministerios $ministerio0
 */
class Organizadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organizadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ministerio', 'actividad'], 'integer'],
            [['ministerio', 'actividad'], 'unique', 'targetAttribute' => ['ministerio', 'actividad']],
            [['actividad'], 'exist', 'skipOnError' => true, 'targetClass' => Actividades::class, 'targetAttribute' => ['actividad' => 'ID']],
            [['ministerio'], 'exist', 'skipOnError' => true, 'targetClass' => Ministerios::class, 'targetAttribute' => ['ministerio' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ministerio' => 'Ministerio',
            'actividad' => 'Actividad',
        ];
    }

    /**
     * Gets query for [[Actividad0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividad0()
    {
        return $this->hasOne(Actividades::class, ['ID' => 'actividad']);
    }

    /**
     * Gets query for [[Ministerio0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMinisterio0()
    {
        return $this->hasOne(Ministerios::class, ['ID' => 'ministerio']);
    }
}
