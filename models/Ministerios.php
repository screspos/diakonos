<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ministerios".
 *
 * @property int $ID
 * @property string|null $nombre
 * @property int|null $lider
 * @property int|null $iglesia
 *
 * @property Actividades[] $actividads
 * @property Discipulos[] $discipulos
 * @property Iglesias $iglesia0
 * @property Discipulos $lider0
 * @property Organizadores[] $organizadores
 * @property ParticipantesMinisterio[] $participantesMinisterios
 */
class Ministerios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ministerios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lider', 'iglesia'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['lider'], 'exist', 'skipOnError' => true, 'targetClass' => Discipulos::class, 'targetAttribute' => ['lider' => 'ID']],
            [['iglesia'], 'exist', 'skipOnError' => true, 'targetClass' => Iglesias::class, 'targetAttribute' => ['iglesia' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'nombre' => 'Nombre',
            'lider' => 'Lider',
            'iglesia' => 'Iglesia',
        ];
    }

    /**
     * Gets query for [[Actividads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividads()
    {
        return $this->hasMany(Actividades::class, ['ID' => 'actividad'])->viaTable('organizadores', ['ministerio' => 'ID']);
    }

    /**
     * Gets query for [[Discipulos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipulos()
    {
        return $this->hasMany(Discipulos::class, ['ID' => 'discipulo'])->viaTable('participantes_ministerio', ['ministerio' => 'ID']);
    }

    /**
     * Gets query for [[Iglesia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIglesia0()
    {
        return $this->hasOne(Iglesias::class, ['ID' => 'iglesia']);
    }

    /**
     * Gets query for [[Lider0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLider0()
    {
        return $this->hasOne(Discipulos::class, ['ID' => 'lider']);
    }

    /**
     * Gets query for [[Organizadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizadores()
    {
        return $this->hasMany(Organizadores::class, ['ministerio' => 'ID']);
    }

    /**
     * Gets query for [[ParticipantesMinisterios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipantesMinisterios()
    {
        return $this->hasMany(ParticipantesMinisterio::class, ['ministerio' => 'ID']);
    }
}
