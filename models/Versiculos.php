<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "versiculos".
 *
 * @property int $ID
 * @property int|null $predica
 * @property string|null $versiculo
 *
 * @property Predicas $predica0
 */
class Versiculos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'versiculos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['predica'], 'integer'],
            [['versiculo'], 'string', 'max' => 30],
            [['predica', 'versiculo'], 'unique', 'targetAttribute' => ['predica', 'versiculo']],
            [['predica'], 'exist', 'skipOnError' => true, 'targetClass' => Predicas::class, 'targetAttribute' => ['predica' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'predica' => 'Predica',
            'versiculo' => 'Versiculo',
        ];
    }

    /**
     * Gets query for [[Predica0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPredica0()
    {
        return $this->hasOne(Predicas::class, ['ID' => 'predica']);
    }
}
