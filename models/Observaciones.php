<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "observaciones".
 *
 * @property int $ID
 * @property int|null $discipulo
 * @property string|null $observacion
 * @property string|null $fecha
 *
 * @property Discipulos $discipulo0
 */
class Observaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'observaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['discipulo'], 'integer'],
            [['observacion'], 'string'],
            [['fecha'], 'safe'],
            [['discipulo'], 'exist', 'skipOnError' => true, 'targetClass' => Discipulos::class, 'targetAttribute' => ['discipulo' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'discipulo' => Yii::t('app', 'Discipulo'),
            'observacion' => Yii::t('app', 'Observacion'),
            'fecha' => Yii::t('app', 'Fecha'),
        ];
    }

    /**
     * Gets query for [[Discipulo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipulo0()
    {
        return $this->hasOne(Discipulos::class, ['ID' => 'discipulo']);
    }
}
