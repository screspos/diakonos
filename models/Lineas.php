<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lineas".
 *
 * @property int $ID
 * @property string|null $nombre
 * @property int|null $iglesia
 * @property int|null $lider
 *
 * @property Actividades[] $actividades
 * @property Celulas[] $celulas
 * @property Iglesias $iglesia0
 * @property Discipulos $lider0
 */
class Lineas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lineas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iglesia', 'lider'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['lider'], 'exist', 'skipOnError' => true, 'targetClass' => Discipulos::class, 'targetAttribute' => ['lider' => 'ID']],
            [['iglesia'], 'exist', 'skipOnError' => true, 'targetClass' => Iglesias::class, 'targetAttribute' => ['iglesia' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'nombre' => 'Nombre',
            'iglesia' => 'Iglesia',
            'lider' => 'Lider',
        ];
    }

    /**
     * Gets query for [[Actividades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividades()
    {
        return $this->hasMany(Actividades::class, ['linea' => 'ID']);
    }

    /**
     * Gets query for [[Celulas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCelulas()
    {
        return $this->hasMany(Celulas::class, ['linea' => 'ID']);
    }

    /**
     * Gets query for [[Iglesia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIglesia0()
    {
        return $this->hasOne(Iglesias::class, ['ID' => 'iglesia']);
    }

    /**
     * Gets query for [[Lider0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLider0()
    {
        return $this->hasOne(Discipulos::class, ['ID' => 'lider']);
    }
}
