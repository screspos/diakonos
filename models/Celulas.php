<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "celulas".
 *
 * @property int $ID
 * @property string|null $nombre
 * @property int|null $lider
 * @property int|null $linea
 * @property int|null $ubicacion
 *
 * @property Actividades[] $actividades
 * @property Discipulos[] $discipulos
 * @property Discipulos $lider0
 * @property Lineas $linea0
 * @property Ubicaciones $ubicacion0
 */
class Celulas extends \yii\db\ActiveRecord
{
    public $nombre_linea, $direccion, $localidad, $provincia, $pais, $nombre_lider;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'celulas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lider', 'linea', 'ubicacion'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['lider'], 'exist', 'skipOnError' => true, 'targetClass' => Discipulos::class, 'targetAttribute' => ['lider' => 'ID']],
            [['linea'], 'exist', 'skipOnError' => true, 'targetClass' => Lineas::class, 'targetAttribute' => ['linea' => 'ID']],
            [['ubicacion'], 'exist', 'skipOnError' => true, 'targetClass' => Ubicaciones::class, 'targetAttribute' => ['ubicacion' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'nombre' => 'Nombre',
            'lider' => 'Lider',
            'linea' => 'Linea',
            'ubicacion' => 'Ubicacion',
        ];
    }

    /**
     * Gets query for [[Actividades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActividades()
    {
        return $this->hasMany(Actividades::class, ['celula' => 'ID']);
    }

    /**
     * Gets query for [[Discipulos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipulos()
    {
        return $this->hasMany(Discipulos::class, ['cod_celula' => 'ID']);
    }

    /**
     * Gets query for [[Lider0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLider0()
    {
        return $this->hasOne(Discipulos::class, ['ID' => 'lider']);
    }

    /**
     * Gets query for [[Linea0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLinea0()
    {
        return $this->hasOne(Lineas::class, ['ID' => 'linea']);
    }

    /**
     * Gets query for [[Ubicacion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUbicacion0()
    {
        return $this->hasOne(Ubicaciones::class, ['ID' => 'ubicacion']);
    }
}
